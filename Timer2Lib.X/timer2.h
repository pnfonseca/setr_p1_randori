/**
 * \file timer2.h
 *
 * \brief Module for configuring and controlling PIC32 Timer2.
 *
 * The module is used as an example of TDD (Test Driven Development) using
 * CppUTest
 *
 * \author Pedro Fonseca (pf@ua.pt)
 */

#pragma once

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

#define PBCLKfreq   40000000L

#include <stdint.h>


/**
 *
 * States for the timer
 *
 * There are two possible states: On and Off
 */
typedef enum TimerStates {
  TOn,    /*!< Timer On */
  TOff    /*!< Timer Off */
} TimerStates_t;

/**
 *
 * \param Prescaler  Prescaler value
 * \param fout       End of count frequency
 *
 *
 *  \return A result code
 *  \retval    0     Success
 *  \retval    -1    Impossible prescaler values
 *
 * \brief
 * Configures Timer2 registers to generate End of Count events at a
 * frequency fout. Requires user to define the prescaler value.
 */
int8_t set_timer2_freq(uint16_t Prescaler, uint32_t fout);

/**
 *  \brief    Configures the Timer2 registers. Receives the values for
 *            PR2 and Prescaler.
 *
 * The Prescaler value must be one of the dividing factors (1, 2, 4, ...)
 * config_timer2 will write the corresponding bit values in TCKPS register.
 *
 *  \param prescaler    Prescaler value
 *  \param ValPR2       Value to store in PR2 register
 *
 *  \return A result code
 *  \retval    0     Success
 *  \retval    -1    Impossible prescaler values
 *
 * \author Pedro Fonseca (pf@ua.pt)
 */
int8_t config_timer2(uint16_t Prescaler, uint16_t ValPR2);

/**
 *  \brief    Controls the state of Timer2 (run/stop)
 *
 *  \param    T2state   Controls the state of Timer2
 *
 * \sa TimerStates_t
 */
void Timer2control(TimerStates_t T2state);


/* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif
