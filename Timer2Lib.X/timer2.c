/*
 * timer2.c
 *
 * Módulo para controlo do Timer2
 */

#ifndef _UNITTESTING_
#include <xc.h>
#else
#include "mem_map.h"
#endif

#include "timer2.h"




int8_t set_timer2_freq(uint16_t Prescaler, uint32_t fout)
{
    int8_t result;

    return result;

}


int8_t config_timer2(uint16_t Prescaler, uint16_t ValPR2)
{

    /* Guarantee that Timer 2 is not running */
    T2CONbits.ON = 0;

    /* Reset the timer counter to zero */
    TMR2 = 0;

    /* Configuration of Timer2 hardware */
    T2CONbits.TGATE = 0;
    T2CONbits.TCS = 0;

    /* Set TCKPS and PR2 */
    int8_t result = -1;
    uint16_t range[8] = {1, 2, 4, 8, 16, 32, 64, 256};

    for(int i=0; i<8; i++)
    {
      if(range[i] == Prescaler)
      {
        result = 0;
        break;
      }
    }

    return result;

}

void Timer2control(TimerStates_t T2state)
{
<<<<<<< Updated upstream
  if(T2state == TOn) {T2CON = 0x8000;}
  else {T2CON = 0x0000;}
=======
  if(T2state == TOn) T2CON = 0x8000;

  else T2CON = 0x0000;

>>>>>>> Stashed changes
	return;
}
