/*
 * testRunner.cpp
 *
 *  Created on: Dec 9, 2012
 *      Author: pf
 */


#include <CppUTest/TestHarness.h>

extern "C" {
#include "timer2.h"
#include "mem_map.h"
}



TEST_GROUP(TimerTest)
{
	void setup()
	{

	}

	void teardown()
	{

	}
};


TEST(TimerTest,SetTimerOn){

	/* Clear all bits in T2CON */
	T2CON = 0x0000;

	Timer2control(TOn);

	CHECK_EQUAL(0x8000, T2CON);

}

TEST(TimerTest,ResetTimer){

	/* Set TON bit */
	T2CONbits.ON = 1;

	Timer2control(TOff);

	CHECK_EQUAL(0x0000, T2CON);

}

/* 1) test the return value:
Prescaler = 128 fails
Prescaler max = 256
Prescaler = [1 2 4 8 16 32 64 256]
Return 0 belongs to prescaler array
Return -1 doesn't belong to prescaler array
*/
TEST(TimerTest, Prescaler_Validation)
{
	uint16_t range[8] = {1, 2, 4, 8, 16, 32, 64, 256};
	uint16_t values[8] =  {3, 5, 7, 17, 37, 128, 300, 500};

	int i;
	for(i=0; i<8; i++)
	{
		CHECK_EQUAL(0, config_timer2(range[i],0));
		CHECK_EQUAL(-1, config_timer2(values[i],0));
	}
}
